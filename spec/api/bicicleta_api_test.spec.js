var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {

    beforeAll(function (done) {
        mongoose.connection.close().then(() => {
            mongoose.connect('mongodb://localhost/test', { 
                useUnifiedTopology: true, 
                useFindAndModify: false,
                useCreateIndex: true,
                useNewUrlParser: true
            });
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it("Status 200", (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toBe(0);
                    done();
                    console.log('OK! api/Bicicletas -> GET : Funciona correctamente');
                });
            })
        })
    })

    describe('POST BICICLETAS /create', () => {
        it("Status 200", (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{ "code": 10, "color": "Black", "modelo": "Orbea", "lat": 42, "lng": 3 }';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                var result = JSON.parse(body);
                console.log(result);
                expect(result.bicicleta.color).toBe('Black');
                expect(result.bicicleta.modelo).toBe("Orbea");
                expect(result.bicicleta.ubicacion[0]).toBe(42);
                expect(result.bicicleta.ubicacion[1]).toBe(3);
                done();
                console.log('OK! api/bicicletas/create -> POST : Funciona correctamente');
            });
        });
    });

