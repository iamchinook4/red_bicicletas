var Usuario = require('../../models/usuario');
var mongoose = require('mongoose');
var request = require('request');
var server = require('../../bin/www');

var urlServer = 'http://localhost:3000/api/usuarios'


describe('test API Usuarios', () => {
    beforeEach(function (done) {
        mongoose.connection.close().then(() => {
            var mongoDB = "mongodb://localhost/testdb";
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {
                console.log('******Se conecto a la db test******');
                done();
            });
        });
    });

    afterEach(function(done) {
        Usuario.deleteMany({}, function (err, success) {  
            if(err) { console.log(err); }
            mongoose.connection.close(done)
        });
    });
    
    describe('GET Usuarios', () => {
        it('Status 200', (done) => {
            request.get(urlServer, function (error, res, body) {  
                console.log(body)
                expect(res.statusCode).toBe(200);
                var result = JSON.parse(body);
                expect(result.usuarios.length).toBe(0);
                done();
                console.log('OK! api/Usuarios -> GET : Funciona correctamente');
            });
        });
    });


    
});
