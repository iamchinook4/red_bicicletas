var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', () => {

    beforeEach(function(done) {
        mongoose.connect('mongodb://localhost/test', { 
            useUnifiedTopology: true, 
            useFindAndModify: false,
            useCreateIndex: true,
            useNewUrlParser: true
        });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Mongoose connection error:'));
        db.once('open', function() {
          console.log('******Se conecto a la db test******');
          done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
            mongoose.disconnect();
        });
    });


    describe('Bicicleta.createInstance', () => {
        it('Crea una Instancia de Bicicleta', () => {
            var a1 = Bicicleta.createInstance(1, 'Black', 'Orbea', [42.266950, 2.956106]);
    
            expect(a1.code).toEqual(1);
            expect(a1.color).toBe('Black');
            expect(a1.modelo).toBe('Orbea');
            expect(a1.ubicacion[0]).toEqual(42.266950);
            expect(a1.ubicacion[1]).toEqual(2.956106);
            console.log('******instancia creada******');
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Bicicletas empieza vacio', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
                console.log('************all bicis correcto*****');
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega una Bicicleta', (done) => {
            var a1 = Bicicleta({code: 1, color: 'Black', modelo: 'Orbea', ubicacion: [42.266950, 2.956106]});
            Bicicleta.add(a1, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(a1.code)
                    done();
                    console.log('******bici creada******');
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la Bicicleta con codigo 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var a1 = Bicicleta({code: 1, color: 'Black', modelo: 'Orbea', ubicacion: [42.266950, 2.956106]});
                Bicicleta.add(a1, function(err, newBici) {
                    if (err) console.log(err);

                    var a2 = Bicicleta({code: 2, color: 'White', modelo: 'Cannon', ubicacion: [41, 3]});
                    Bicicleta.add(a2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici) {
                            console.log('findbyCode: ' + targetBici);
                            expect(targetBici.code).toBe(a1.code);
                            expect(targetBici.color).toBe(a1.color);
                            expect(targetBici.modelo).toBe(a1.modelo);
                            done();
                            console.log('******busqueda por codigo correcta******');
                        })
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('Debe devolver la Bicicleta con codigo 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var a1 = Bicicleta({code: 1, color: 'Black', modelo: 'Orbea', ubicacion: [42.266950, 2.956106]});
                Bicicleta.add(a1, function(err, newBici) {
                    if (err) console.log(err);

                    var a2 = Bicicleta({code: 2, color: 'White', modelo: 'Cannon', ubicacion: [41, 3]});
                    Bicicleta.add(a2, function(err, newBici) {
                        if (err) console.log(err);

                        Bicicleta.removeByCode(1, function (err) {
                            if (err) console.log(err);

                            Bicicleta.allBicis(function(err, bicis) {
                                expect(bicis.length).toBe(1);
                                
                                Bicicleta.findByCode(2, function (err, targetBici) {
                                    if (err) console.log(err);
                                    expect(targetBici.code).toBe(a2.code);
                                    expect(targetBici.color).toBe(a2.color);
                                    expect(targetBici.modelo).toBe(a2.modelo);
                                    done();
                                    console.log('******remove by code correcto******');
                                });
                            });
                        });
                    });
                });
            });
        });
    });

});

