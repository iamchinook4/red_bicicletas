var Bicicleta = require('../../models/bicicleta');


exports.bicicleta_list = (req, res) => {
    Bicicleta.allBicis(function (err, allBicis) {
        res.status(200).json({ bicicletas: allBicis });
    })
}

exports.bicicleta_create = (req, res) => {
    const bici = new Bicicleta({
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    });
    Bicicleta.add(bici, (err, newBici) => {
        if (err){res.status(500)}
        else{
            res.status(200).json({
                bicicleta: newBici
            });
        }
    });
}

exports.bicicleta_update = (req, res) => {
    const bici={
        _id: req.body._id,
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    };

    Bicicleta.findByIdAndUpdate(bici._id, bici, function(err, update) {
        if (err){ res.status(500) }
        else{
            res.status(200).json({
                bici: bici
            });
        }
    });
}

exports.bicicleta_delete = (req, res) => {
    Bicicleta.removeByCode(req.body.code,  (err, deletedBici) => {
        if (err){ res.status(500) }
        else{ res.status(204).send(); }
    });
    
}