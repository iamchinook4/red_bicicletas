var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function (req, res){
    Bicicleta.allBicis((err, bicicletas) => { 
        res.render('bicicletas/index', {bicis: bicicletas});
    });
}

exports.bicicleta_create_get=(req, res)=>{
    res.render('bicicletas/create');
}

exports.bicicleta_create_post= function(req, res){    
    var bici = new Bicicleta({ code: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng] });
    // bici.ubicacion=[req.body.lat, req.body.lng];
    Bicicleta.add(bici);    
    res.redirect('/bicicletas'); 
}

exports.bicicleta_update_get = function (req, res){
    Bicicleta.findById(req.params.id, function(err, bici) {
        res.render('bicicletas/update', { errors: {}, bici: bici} );
    });
}

exports.bicicleta_update_post= function(req, res) {
    var bicicletaUpdate = { code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng] };
    Bicicleta.findByIdAndUpdate(req.params.id, bicicletaUpdate, ( err, bicicleta ) => {
        if (err) {
            console.log(err);
            res.render('bicicletas/update', {errors: erro.errors, bicicleta: new Bicicleta({ 
                code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng] } )});
        }
        else {
            res.redirect('/bicicletas');
            return;
        }
    }); 
}

exports.bicicleta_delete_post= function(req, res, next){
    Bicicleta.findByIdAndDelete(req.body.id, function(err) {
        if (err) next(err);
        else res.redirect('/bicicletas');
    });
}

