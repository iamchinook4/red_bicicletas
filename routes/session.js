var express = require('express');
var router = express.Router();
var Usuario = require('../models/usuario');
var Token = require('../models/token');
const passport = require('../config/passport');

router.get('/login', function(req, res) {
res.render('session/login');
});

router.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, usuario, info) {
        if (err) return next(err);
        if (!usuario) return res.render('session/login', { info });
        req.login(usuario, function(err) {
        if (err) return next(err);
        return res.redirect('/bicicletas');
        });
    })(req, res, next);
});

router.get('/logout', function(req, res) {
    req.logout(); 
    res.redirect('/session/login');
});

router.get('/forgotPassword', function(req, res) {
    res.render('/session/forgotPassword');
});

router.post('/forgotPassword', function(req, res) {
    Usuario.findOne( { email: req.body.email }, function (err, usuario) {
        if (!usuario) return res.render('session/forgotPassword', {info: {message: 'Correo invalido'}});
        usuario.resetPassword(function(err) {
            if (err) return next(err);
            console.log('session/forgotPasswordMessage');
        });
        res.render('/session/forgotPasswordMessage');
    });
});

router.get('/resetPassword/:token', function(req, res, next) {
    Token.findOne({ token: req.params.token }, function ( err, token ) {
        if (!token) return res.status(400).send( { type: 'not-verifified', msg: 'Usuario asociado al token inexistente'});
        Usuario.findById(token._userId, function (err, usuario) {
            if (!usuario) return res.status(400).send( { msg: 'No existe un usuario asociado al token'});
            res.render('/session/resetPassword', { errors: {}, usuario: usuario});
        });
    });
});

router.post('/resetPassword', function(req, res) {
    if (req.body.password != req.body.confirm_password) {
        res.render('/session/resetPassword', {
            errors: {
                confirm_password: {
                    message: 'Contraseña incorrecta'
                }
            },
            usuario: new Usuario({ email: req.body.email })
        });
        return;
    }
    Usuario.findOne({ email: req.body.email }, function ( err, usuario ) {
        usuario.password = req.body.password; 
        usuario.save(function(err) {
            if (err) {
                res.render('resetPassword', {errors: err.errors, usuario: new Usuario({ email: req.body.email })});
            }
            else {
                res.redirect('/login');
            }
        });
    });
});


module.exports = router;