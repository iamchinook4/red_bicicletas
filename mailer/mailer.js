require('dotenv').config();
const nodemailer = require('nodemailer')
const nodemailerSendgrid = require('nodemailer-sendgrid');

let mailConfig

if (process.env.NODE_ENV === "production") {

    const transport = nodemailer.createTransport(
        nodemailerSendgrid({
            apiKey: process.env.SENDGRID_API_SECRET
        })
    );
    module.exports = transport;
} else {
    if (process.env.NODE_ENV === "staging") {

        const transport = nodemailer.createTransport(
            nodemailerSendgrid({
                apiKey: process.env.SENDGRID_API_SECRET
            })
        );
        module.exports = transport;
    } else {
        mailConfig = {
            host: "smtp.ethereal.email",
            port: 587,
            auth: {
                user: process.env.ETHEREAL_USER_DEV,
                pass: process.env.ETHEREAL_PASS_DEV
            },
        }
        module.exports = nodemailer.createTransport(mailConfig)

    }
}

// module.exports = nodemailer.createTransport(mailConfig)