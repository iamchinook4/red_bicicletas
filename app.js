// require('newrelic');
require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const Token = require('./models/token');
const Usuario = require('./models/usuario');
const passport = require('./config/passport');
const session = require('express-session');
const mongoDBStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var sessionsRouter = require('./routes/session');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token')
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var authAPIRouter = require('./routes/api/auth');


// const store = new session.MemoryStore;
let store;
if (process.env.NODE_ENV === "development") {
  store = new session.MemoryStore
} else {
  store = new mongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  })
  store.on('error', (error) => {
    assert.ifError(error)
    assert.ok(false)
  })
}

var app = express();
app.set('secretKey', 'jwt_pwd_!1223344');
app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'Red_bicis,!2oO0'
}));

const mongoose = require('mongoose');
// Base de datos Local - Mongo Compass
// var mongoDB = 'mongodb://localhost/red_bicicletas';

// Base de datos Remota - Mongo Atlas
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, {
  useUnifiedTopology: true, 
  useFindAndModify: false,
  useCreateIndex: true,
  useNewUrlParser: true
});
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongoose connection error:'));
db.once('open', function() {
  console.log('Connected to MongoDB');
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/session', sessionsRouter);
app.use('/usuarios', loggedIn, usuariosRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);
app.use('/api/auth', authAPIRouter);

app.use('/privacy_policy',function(req,res){
  res.sendFile('public/privacy_policy.html');
});

app.use('/google8dfeb5ad9e167dd4',function(req,res){
  res.sendFile('public/google8dfeb5ad9e167dd4.html');
});

app.get('/auth/google',
  passport.authenticate('google', { scope:[
    'https://www.googleapis.com/auth/plus.login',
    // 'https://www.googleapis.com/auth/plus.profile.emails.read'
    //'email', //todavia no agrego el profe
    //'profile' //todavia no agrego el profe
    ]}
));

app.get( '/auth/google/callback',
    passport.authenticate( 'google', {
        successRedirect: '/',
        failureRedirect: '/error'
}));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {
  if (req.user) { //guarda los datos del usuario en el request
    next();
  }
  else {
    console.log('Usuario sin loguearse');
    res.redirect('/session/login');
  }
};

function validarUsuario(req, res, next) { 
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
    if (err) {
      console.log(err);
      res.json({status:"error", message: err.message, date:null});
    }
    else {
      req.body.userId = decoded.id;
      console.log('jwt verify: ' + decoded);
      next();
    }
  });
}

module.exports = app;
